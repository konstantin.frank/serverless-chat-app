package com.xitaso.kfbachelorthesis;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.List;
import java.util.Random;

/**
 *  The client calls this lambda function in order to get the name of his SQS queue on successfull login.
 *
 *  The names of the queues are generated in the form username-SOMEHASH.
 *  Since the queues are listed by prefix, this is the reason why usernames may not contain the character '-'
 *
 *  If no queue of that user is found, a new one is created.
 *
 *  For a real production environment it would probably make sense to delete the old queue and create a new one
 *  with a new hash every time this is requested.
 *
 *  NOTE: there is actually a 80 character limit on queue names - so avoid extremely large project names.
 *  Knowing this, storing the queue name in the database instead of working with prefix might be a good idea.
 */

public class ReturnSQSQueueHandler implements RequestStreamHandler{

    private final String region = "eu-west-1"; // unfortunately fifo-queues are not available in frankfurt (eu-central-1), so we hardcoded this here!

    // environment variables managed by lambda execution environment (and uploaded by CloudFormation template)
    private String projectName;

    JSONParser parser;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        projectName = System.getenv("PROJECT_NAME");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        String sqsQueryName = null;

        try {
            String username = getUsernameFromJson(reader);

            AmazonSQS sqs = AmazonSQSClientBuilder.standard()
                    .withRegion(Regions.fromName(region))
                    .build();

            String queueName = findQueue(username, sqs);
            if (queueName == null) {
                queueName = createQueue(username, sqs);
            } else {
                queueName = parseUrlToQueueName(queueName); //findQueue returns an url, not the name!
            }
            try {
                //we need to purge the queue because the client always fetches all previous messages from dynamodb!
                PurgeQueueRequest purgeRequest = new PurgeQueueRequest(queueName);
                sqs.purgeQueue(purgeRequest);
            } catch (PurgeQueueInProgressException e) {
                System.out.println("WARNING: could not empty queue - messages may be retrieved twice (see thesis for further details");
                e.printStackTrace();
            }

            System.out.println("QueueName is "+queueName);

            transferQueueNameToResponseJson(responseJson, queueName);

        } catch(ParseException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        } catch (AmazonSQSException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }

        System.out.println(responseJson.toJSONString());

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private String getUsernameFromJson(BufferedReader reader) throws ParseException, IOException {
        parser = new JSONParser();
        JSONObject event = (JSONObject) parser.parse(reader);
        System.out.println(event.toString());

        JSONObject requestContext = (JSONObject) event.get("requestContext");
        JSONObject authorizer = (JSONObject) requestContext.get("authorizer");
        JSONObject claims = (JSONObject) authorizer.get("claims");
        return (String) claims.get("cognito:username");
    }

    private String findQueue(String username, AmazonSQS sqs) throws AmazonSQSException{
        // list queues with prefix username
        ListQueuesResult result = sqs.listQueues(new ListQueuesRequest(username+"-" + projectName));

        // there should be only one result - if there are more, a illegal username is present!
        List<String> queueUrls = result.getQueueUrls();
        if (queueUrls.isEmpty()) {
            return null;
        } else {
            return queueUrls.get(0);
        }
        // you could add an error case for more than one result (see class description)

    }

    private String createQueue(String username, AmazonSQS sqs) throws AmazonSQSException {
        String queueName = username + "-" + projectName + createSomeRandomString() + ".fifo";
        // instead of writing the username inf front of the queue you could also store the queue name in dynamodb
        CreateQueueRequest createRequest = new CreateQueueRequest(queueName)
                .addAttributesEntry("MessageRetentionPeriod", "86400")
                .addAttributesEntry("FifoQueue", "true")
                .addAttributesEntry("ReceiveMessageWaitTimeSeconds", "20") //enable long-poll
                .addAttributesEntry("ContentBasedDeduplication", "true");

        sqs.createQueue(createRequest);
        return queueName;
    }

    /**
     * Generate some simple pseudo-random string
     * @return a random string (20 characters, currently only values of 0-9)
     */
    private String createSomeRandomString() {
        Random rand = new Random();
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 20; i++) {
            builder.append(rand.nextInt(10));
        }
        return builder.toString();
    }

    private void transferQueueNameToResponseJson(JSONObject responseJson, String sqsQueueName) {
        JSONObject responseBody = new JSONObject();
        responseBody.put("sqsQueueName", sqsQueueName);

        responseJson.put("statusCode", "200");
        responseJson.put("isBase64Encoded", false);
        responseJson.put("body", responseBody.toString());
    }

    private String parseUrlToQueueName(String queueUrl) {
        // the queueUrl looks like this: https://sqs.eu-west-1.amazonaws.com/528105980221/User-002261168077032505744184673162.fifo
        String[] splitedUrl = queueUrl.split("/");
        return splitedUrl[splitedUrl.length - 1];
    }

}
