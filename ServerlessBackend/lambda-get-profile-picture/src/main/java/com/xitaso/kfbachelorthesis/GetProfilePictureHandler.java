package com.xitaso.kfbachelorthesis;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.xitaso.kfbachelorthesis.lambda.model.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

/**
 *  The client calls this lambda function in order to get the s3-location of the profile picture of a user.
 *
 *  It also uses this to find out whether a user exists or not.
 *
 *  This lambda function could also be used for access to other files, like sent images - however,
 *  if you do this, you should grant the user only temporary access (e.g. with cognito credentials)
 *  since those pictures are not public (unlike profile pictures)
 */

public class GetProfilePictureHandler implements RequestStreamHandler{

    // environment variables managed by lambda runtime (specified in CloudFormation template)
    private String region;
    private String userTableName;

    JSONParser parser;
    private AmazonDynamoDB dynamoDBclient;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();

        // set environment variables
        region = System.getenv("REGION");
        userTableName = System.getenv("USER_TABLE_NAME");

        try {
            String username = getQueryUsernameFromJson(reader);

            initDynamoDBClient();

            // if no user was found, this is simply going to be null
            String s3bucketName = findS3BucketName(username);

            System.out.println("Resource name is "+ s3bucketName);

            transferBucketNameToResponseJson(responseJson, s3bucketName);

        } catch(ParseException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }
        System.out.println("Response is: " + responseJson.toJSONString());

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private void initDynamoDBClient() {
        dynamoDBclient = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.fromName(region))
                .build();
    }

    private String findS3BucketName(String username) {
        System.out.println("Trying to get s3 bucket name for "+username);
        DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(userTableName))
                .build();
        DynamoDBMapper mapper = new DynamoDBMapper(dynamoDBclient, mapperConfig);

        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":v1", new AttributeValue().withS(username));

        DynamoDBQueryExpression<User> queryExpression = new DynamoDBQueryExpression<User>()
                .withKeyConditionExpression("Username = :v1")
                .withExpressionAttributeValues(expressionAttributeValues);

        List<User> userList = new ArrayList<User>(mapper.query(User.class, queryExpression));

        if (userList.isEmpty()) {
            System.out.println("Userlist was empty");
            return null;
        }
        return userList.get(0).getProfilePictureLocation();

    }

    private String getQueryUsernameFromJson(BufferedReader reader) throws ParseException, IOException {
        parser = new JSONParser();
        JSONObject event = (JSONObject) parser.parse(reader);
        JSONObject queryStringParameters = (JSONObject) event.get("queryStringParameters");
        return queryStringParameters.get("username").toString();
    }

    private void transferBucketNameToResponseJson(JSONObject responseJson, String bucketName) {
        JSONObject responseBody = new JSONObject();
        responseJson.put("statusCode", "200");
        responseJson.put("isBase64Encoded", false);
        if (bucketName == null) {
            responseBody.put("userExisting", "false");
        } else {
            responseBody.put("userExisting", "true");
            responseBody.put("s3objectName", bucketName);
        }
        responseJson.put("body", responseBody.toString());
    }
}
