package com.xitaso.kfbachelorthesis;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
public class CognitoTriggerEvent { // custom event, the CognitoEvent refers to Cognito Sync!
    private int version;
    private String triggerSource;
    private String region;
    private String userPoolId;
    private String userName;
    private Map<String, String> callerContext;
    private Request request;
    private Response response;

    @Getter
    @Setter
    @NoArgsConstructor
    @JsonSerialize
    public static class Request {
        private Map<String, String> userAttributes;
        public Request(Map<String, String> userAttr) {
            userAttributes = userAttr;
        }
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @JsonSerialize
    public static class Response { }
}
