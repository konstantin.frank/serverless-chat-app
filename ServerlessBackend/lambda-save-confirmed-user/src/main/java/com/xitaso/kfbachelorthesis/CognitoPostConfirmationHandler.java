package com.xitaso.kfbachelorthesis;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Date;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.xitaso.kfbachelorthesis.lambda.model.User;

/**
 * Save user data in a dynamodb table after he has been confirmed through cognito
 */
public class CognitoPostConfirmationHandler implements RequestHandler<CognitoTriggerEvent, CognitoTriggerEvent>{

    // environment variables managed by lambda runtime (specified in CloudFormation template)
    private String userTableName;
    private String region;

    private AmazonDynamoDB client;

    public CognitoTriggerEvent handleRequest(CognitoTriggerEvent event, Context context) {

        // get environment variables
        region = System.getenv("REGION");
        userTableName = System.getenv("USER_TABLE_NAME");

        initDynamoDbClient();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        User user = new User();
        user.setUsername(event.getUserName());
        user.setEmail(event.getRequest().getUserAttributes().get("email"));
        user.setLastOnlineDate(new Date());
        user.setOnline(false);

        DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(userTableName))
                .build();
        DynamoDBMapper mapper = new DynamoDBMapper(client, mapperConfig);
        mapper.save(user);

        try {
            System.out.println("Got event: "+ ow.writeValueAsString(event));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return event;
    }

    private void initDynamoDbClient() {
        client = AmazonDynamoDBClientBuilder
                    .standard()
                    .withRegion(Regions.fromName(region))
                    .build();
    }

}

