package com.xitaso.kfbachelorthesis.lambda.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.Date;

@DynamoDBTable(tableName = "Messages")
public class Message {

    private String senderName;
    private String receiverName;
    private Date dateSent;
    private String text;

    @DynamoDBHashKey(attributeName = "SenderName")
    public String getSenderName() {return senderName;}
    public void setSenderName(String senderName) {this.senderName = senderName;}

    @DynamoDBAttribute(attributeName = "ReceiverName")
    public String getReceiverName() {return receiverName;}
    public void setReceiverName(String receiverName) {this.receiverName = receiverName;}

    @DynamoDBRangeKey(attributeName = "DateSent")
    public Date getDateSent() {return dateSent; }
    public void setDateSent(Date dateSent) {this.dateSent = dateSent; }

    @DynamoDBAttribute(attributeName = "Text")
    public String getText() {return text;}
    public void setText(String text) {this.text = text;}

}
