package com.xitaso.kfbachelorthesis.lambda.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.Date;

@DynamoDBTable(tableName = "Users")
public class User {

    private String username;
    private String email;
    private Date lastOnlineDate;
    private boolean online;
    private String profilePictureLocation;

    @DynamoDBHashKey(attributeName = "Username")
    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}

    @DynamoDBAttribute(attributeName = "Email")
    public String getEmail() {return email;}
    public void setEmail(String email) {this.email = email;}

    @DynamoDBAttribute(attributeName = "LastOnlineDate")
    public Date getLastOnlineDate() {return lastOnlineDate;}
    public void setLastOnlineDate(Date lastOnlineDate) {this.lastOnlineDate = lastOnlineDate;}

    @DynamoDBAttribute(attributeName = "Online")
    public boolean getOnline() {return online;}
    public void setOnline(boolean online) {this.online = online;}

    @DynamoDBAttribute(attributeName = "ProfilePictureLocation")
    public String getProfilePictureLocation() {return profilePictureLocation;}
    public void setProfilePictureLocation(String profilePictureLocation) {this.profilePictureLocation = profilePictureLocation;}

}
