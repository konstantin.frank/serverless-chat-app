package com.xitaso.kfbachelorthesis;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.CreateUserPoolDomainRequest;
import com.amazonaws.services.cognitoidp.model.DeleteUserPoolDomainRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 *  This lambda function provides custom CloudFormation logic for adding a domain to a Cognito user pool
 *  as a workaround since CloudFormation does not yet support this feature of user pools.
 */

public class CreateUserPoolDomainHandler implements RequestStreamHandler{

    JSONParser parser;
    private final String REGION = "eu-central-1";

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        parser = new JSONParser();

        try {
            JSONObject event = (JSONObject) parser.parse(reader);
            System.out.println(event.toJSONString());

            String userPoolId = getUserPoolIdFromJson(event);
            String domainName = getDomainNameFromJson(event);
            String requestType = getRequestTypeFromJson(event);
            String preSignedS3Url = getPreSignedS3Url(event);


            if (requestType.equals("Create")) {
                createDomain(domainName, userPoolId);
            } else if (requestType.equals("Delete")) {
                deleteDomain(domainName, userPoolId);
            }

            // we will have to update a response to the provided URL.
            JSONObject responseForS3Url = createCustomResourceProviderResponse(event, domainName);
            responseForS3Url.put("Status", "SUCCESS"); // if CloudFormation receives SUCCESS, it will continue with stack creation. Otherwise it will rollback it.
            uploadResponseToPreSignedS3Url(preSignedS3Url, responseForS3Url);

            responseJson.put("statusCode", "200");

        } catch(Exception e) {
            try {
                JSONObject event = (JSONObject) parser.parse(reader);
                String preSignedS3Url = getPreSignedS3Url(event);
                JSONObject responseForS3Url = createCustomResourceProviderResponse(event, "error");
                responseForS3Url.put("Status", "FAILED"); // if CloudFormation receives SUCCESS, it will continue with stack creation. Otherwise it will rollback it.
                responseForS3Url.put("Reason", e.getMessage());
                uploadResponseToPreSignedS3Url(preSignedS3Url, responseForS3Url);

            } catch (Exception ex) {
                e.printStackTrace();
            }
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private String getUserPoolIdFromJson(JSONObject event) {
        JSONObject resourceProperties = (JSONObject) event.get("ResourceProperties");
        return (String) resourceProperties.get("UserPoolId");
    }

    private String getDomainNameFromJson(JSONObject event) {
        JSONObject resourceProperties = (JSONObject) event.get("ResourceProperties");
        return (String) resourceProperties.get("DomainName");
    }

    private String getRequestTypeFromJson(JSONObject event) {
        return (String) event.get("RequestType");
    }

    private String getPreSignedS3Url(JSONObject event) {
        return (String) event.get("ResponseURL");
    }

    private void createDomain(String domainName, String userPoolId) {
        AWSCognitoIdentityProvider identityProvider = buildAWSCognitoIdentityProvider();

        CreateUserPoolDomainRequest request = new CreateUserPoolDomainRequest()
                .withDomain(domainName)
                .withUserPoolId(userPoolId);

        identityProvider.createUserPoolDomain(request);
    }

    private void deleteDomain(String domainName, String userPoolId) {
        AWSCognitoIdentityProvider identityProvider = buildAWSCognitoIdentityProvider();

        DeleteUserPoolDomainRequest request = new DeleteUserPoolDomainRequest()
                .withDomain(domainName)
                .withUserPoolId(userPoolId);

        identityProvider.deleteUserPoolDomain(request);
    }

    /*
    We have to upload a response to a pre signed s3 url
     */
    private void uploadResponseToPreSignedS3Url(String preSignedUrl, JSONObject response) throws ProtocolException, IOException {

        URL url = new URL(preSignedUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", "");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(response.toJSONString());
        out.close();
        connection.getResponseCode();
        System.out.println("HTTP response code from preSignedS3Url: " + connection.getResponseCode());
    }

    private JSONObject createCustomResourceProviderResponse(JSONObject event, String domainName) {
        JSONObject response = new JSONObject();
        response.put("PhysicalResourceId", domainName);
        response.put("StackId", event.get("StackId"));
        response.put("RequestId", event.get("RequestId"));
        response.put("LogicalResourceId", event.get("LogicalResourceId"));
        return response;
    }

    private AWSCognitoIdentityProvider buildAWSCognitoIdentityProvider() {
        return AWSCognitoIdentityProviderClientBuilder
                .standard()
                .withRegion(REGION)
                .build();
    }
}
