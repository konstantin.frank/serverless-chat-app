package com.xitaso.kfbachelorthesis;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.xitaso.kfbachelorthesis.lambda.model.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.util.*;

/**
 *  The client calls this lambda function to upload a profile picture.
 *  The profile picture is transferred via invoke payload
 *
 *  This lambda function could also be used for uploading other files, like sent images.
 *
 *  NOTE: lambda functions can currently only be invoked with a payload of 6MB.
 *  If larger items should be uploaded, this lambda function could return a s3bucket location and grant the user temporary write access to it.
 */

public class UploadProfilePictureHandler implements RequestStreamHandler{

    // environment variables managed by lambda runtime (specified in CloudFormation template)
    private String region;
    private String userTableName;
    private String fileBucketName;

    JSONParser parser;
    private AmazonS3 s3client;
    private AmazonDynamoDB dynamoDBclient;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        // set environment variables
        region = System.getenv("REGION");
        userTableName = System.getenv("USER_TABLE_NAME");
        fileBucketName = System.getenv("FILE_BUCKET_NAME");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();

        try {
            parser = new JSONParser();
            JSONObject event = (JSONObject) parser.parse(reader);

            String username = getUsernameFromJson(event);

            initS3Client();
            initDynamoDBClient();

            String s3key = "ProfilePictures/" + createSomeRandomString();
            uploadPictureToS3(event, s3key);

            saveKeyInDynamoDB(s3key, username);

            responseJson.put("statusCode", "200");

        } catch(Exception e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }
        System.out.println("Response is: " + responseJson.toJSONString());

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private void initS3Client() {
        s3client = AmazonS3ClientBuilder
                .standard()
                .withRegion(region)
                .build();
    }

    private void initDynamoDBClient() {
        dynamoDBclient = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.fromName(region))
                .build();
    }

    private void uploadPictureToS3(JSONObject event, String objectKey) throws AmazonS3Exception, IOException {
        InputStream pictureInputStream = new ByteArrayInputStream( event.get("body").toString().getBytes() );
        PutObjectRequest putRequest = new PutObjectRequest(fileBucketName, objectKey, pictureInputStream, new ObjectMetadata());
        PutObjectResult putResult = s3client.putObject(putRequest);
        System.out.println("Putresult " + putResult.toString());
    }

    private String createSomeRandomString() {
        Random rand = new Random();
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 50; i++) {
            builder.append(rand.nextInt(10));
        }
        return builder.toString();
    }

    private void saveKeyInDynamoDB(String s3key, String username) {
        DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(userTableName))
                .build();
        DynamoDBMapper mapper = new DynamoDBMapper(dynamoDBclient, mapperConfig);
        User user = new User();
        user.setUsername(username);
        user.setProfilePictureLocation(s3key);

        DynamoDBMapperConfig dynamoDBMapperConfig = DynamoDBMapperConfig.builder().withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES).build();
        mapper.save(user, dynamoDBMapperConfig);
    }

    private String getUsernameFromJson(JSONObject event) {
        JSONObject requestContext = (JSONObject) event.get("requestContext");
        JSONObject authorizer = (JSONObject) requestContext.get("authorizer");
        JSONObject claims = (JSONObject) authorizer.get("claims");
        return (String) claims.get("cognito:username");
    }
}
