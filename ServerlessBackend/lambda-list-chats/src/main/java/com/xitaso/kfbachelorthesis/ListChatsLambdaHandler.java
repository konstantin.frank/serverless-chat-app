package com.xitaso.kfbachelorthesis;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.lambda.runtime.Context;

import com.xitaso.kfbachelorthesis.lambda.model.Message;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;



public class ListChatsLambdaHandler implements RequestStreamHandler {

    // environment variables managed by lambda runtime (specified in CloudFormation template)
    private String messageTableName;
    private String region;
    private String secondaryIndexName;

    JSONParser parser = new JSONParser();

    private AmazonDynamoDB client;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        // load environment variables
        messageTableName = System.getenv("MESSAGE_TABLE_NAME");
        region = System.getenv("REGION");
        secondaryIndexName = System.getenv("SECONDARY_INDEX_NAME");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        String username = null;

        try {

            JSONObject event = (JSONObject)parser.parse(reader);
            System.out.println(event.toString());

            JSONObject requestContext = (JSONObject) event.get("requestContext");
            JSONObject authorizer = (JSONObject) requestContext.get("authorizer");
            JSONObject claims = (JSONObject) authorizer.get("claims");
            username = (String) claims.get("cognito:username");

            List<Message> messages ;
            initDynamoDbClient();
            messages = queryMessages(username);

            transferMessagesToResponseJson(responseJson, messages);

        } catch(ParseException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }

        System.out.println(responseJson.toJSONString());

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private List<Message> queryMessages(String username) {
        DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(messageTableName))
                .build();
        DynamoDBMapper mapper = new DynamoDBMapper(client, mapperConfig);

        // first query
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":v1", new AttributeValue().withS(username));

        DynamoDBQueryExpression<Message> queryExpressionSender = new DynamoDBQueryExpression<Message>()
                .withKeyConditionExpression("SenderName = :v1")
                .withExpressionAttributeValues(expressionAttributeValues);

        List<Message> messageList = new ArrayList<Message>(mapper.query(Message.class, queryExpressionSender));

        // second query (simple OR query not possible)
        expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":v1", new AttributeValue().withS(username));

        DynamoDBQueryExpression<Message> queryExpressionReceiver = new DynamoDBQueryExpression<Message>()
                .withKeyConditionExpression("ReceiverName = :v1")
                .withIndexName(secondaryIndexName)
                .withExpressionAttributeValues(expressionAttributeValues)
                .withConsistentRead(false); // secondary indices dont support consistent read

        messageList.addAll(mapper.query(Message.class, queryExpressionReceiver));

        return messageList;
    }

    private void initDynamoDbClient() {
        client = AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.fromName(region))
                .build();
    }

    private void transferMessagesToResponseJson(JSONObject responseJson, List<Message> messages) {
        JSONObject responseBody = new JSONObject();
        responseBody.put("messageCount", messages.size());

        JSONArray messageJSONList = new JSONArray();
        for (int i = 0; i < messages.size(); i++) {
            JSONObject messageEntry = new JSONObject();
            JSONObject messageAttributes = new JSONObject();
            messageAttributes.put("senderName", messages.get(i).getSenderName());
            messageAttributes.put("receiverName", messages.get(i).getReceiverName());
            messageAttributes.put("dateSent", messages.get(i).getDateSent().toString());
            messageAttributes.put("text", messages.get(i).getText());

            messageJSONList.add(messageAttributes.toJSONString());
        }
        responseBody.put("messageList", messageJSONList);
        responseJson.put("statusCode", "200");
        responseJson.put("isBase64Encoded", false);
        responseJson.put("body", responseBody.toString());
    }
}
