package com.xitaso.kfbachelorthesis;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.xitaso.kfbachelorthesis.lambda.model.Message;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 *  The client calls this lambda function to send a message.
 *
 *  The function will copy his message to the database (e.g. for analytics)
 *  and forward it to his sqs queue.
 *
 */

public class SendMessageHandler implements RequestStreamHandler{

    // environment variables managed by lambda runtime (specified in CloudFormation template)
    private String messageTableName;
    private String region;
    private String projectName;


    private final String sqsQueueRegion = "eu-west-1"; // unfortunately fifo-queues are not available in frankfurt (eu-central-1), so we hardcode this here


    JSONParser parser;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        // load environment variables
        messageTableName = System.getenv("MESSAGE_TABLE_NAME");
        region = System.getenv("REGION");
        projectName = System.getenv("PROJECT_NAME");


        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        String sqsQueryName = null;

        try {
            parser = new JSONParser();
            JSONObject event = (JSONObject) parser.parse(reader);
            System.out.println(event.toString());


            String username = getUsernameFromJson(event);
            Message messageToSend = getMessageFromJson(event, username, parser);

            AmazonSQS sqs = AmazonSQSClientBuilder.standard()
                    .withRegion(Regions.fromName(sqsQueueRegion))
                    .build();
            String queueName = findQueue(messageToSend.getReceiverName(), sqs);
            System.out.println("QueueName is "+queueName);
            System.out.println("Message: " +messageToSend.getReceiverName() + " "+messageToSend.getSenderName()
            + " " +messageToSend.getText() + " "+ messageToSend.getDateSent());

            transferMessageToDynamoDB(messageToSend);
            forwardMessageToSQS(messageToSend, sqs, queueName);

            responseJson.put("statusCode", "200");

        } catch(ParseException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        } catch (AmazonSQSException e) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", e);
        }

        System.out.println(responseJson.toJSONString());

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();
    }

    private String getUsernameFromJson(JSONObject event) throws ParseException, IOException {
        JSONObject requestContext = (JSONObject) event.get("requestContext");
        JSONObject authorizer = (JSONObject) requestContext.get("authorizer");
        JSONObject claims = (JSONObject) authorizer.get("claims");
        return (String) claims.get("cognito:username");
    }

    private Message getMessageFromJson(JSONObject event, String username, JSONParser parser) throws ParseException, IOException {
        String bodyString = (String) event.get("body");
        JSONObject body = (JSONObject) parser.parse(bodyString);
        String receiverName = body.get("receiverName").toString();
        String text = body.get("messageBody").toString();

        Message message = new Message();
        message.setDateSent(new Date());
        message.setReceiverName(receiverName);
        message.setSenderName(username);
        message.setText(text);

        return message;
    }

    private String findQueue(String receiverName, AmazonSQS sqs) throws AmazonSQSException{
        // list queues with prefix username-projectname
        System.out.println("Filtering queues with "+ receiverName + "-" + projectName);
        ListQueuesResult result = sqs.listQueues(new ListQueuesRequest(receiverName + "-" + projectName));

        // there should be only one result - if there are more, a illegal username is present!
        List<String> queueUrls = result.getQueueUrls();
        if (queueUrls.isEmpty()) {
            return null;
        } else {
            return queueUrls.get(0);
        }
        // you could add an error case for more than one result (see class description)

    }

    private AmazonDynamoDB initDynamoDbClient() {
        return AmazonDynamoDBClientBuilder
                .standard()
                .withRegion(Regions.fromName(region))
                .build();
    }

    /**
     * Generate some simple pseudo-random string
     * @return a random string (30 characters, currently only values of 0-9)
     */
    private String createSomeRandomString() {
        Random rand = new Random();
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 30; i++) {
            builder.append(rand.nextInt(10));
        }
        return builder.toString();
    }

    private void transferMessageToDynamoDB(Message message) {
        DynamoDBMapperConfig mapperConfig = new DynamoDBMapperConfig.Builder()
                .withTableNameOverride(DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(messageTableName))
                .build();
        AmazonDynamoDB dynamoDBclient = initDynamoDbClient();
        DynamoDBMapper mapper = new DynamoDBMapper(dynamoDBclient, mapperConfig);
        mapper.save(message);
    }

    private void forwardMessageToSQS(Message message, AmazonSQS sqs, String sqsQueueName) {
        MessageAttributeValue msgAttribValue = new MessageAttributeValue()
                .withStringValue(message.getSenderName())
                .withDataType("String");
        HashMap<String, MessageAttributeValue> msgAttributesMap = new HashMap<String, MessageAttributeValue>();
        msgAttributesMap.put("senderName", msgAttribValue);

        SendMessageRequest request = new SendMessageRequest()
                .withQueueUrl(sqsQueueName)
                .withMessageBody(message.getText())
                .withMessageAttributes(msgAttributesMap)
                .withMessageGroupId("message"); // all messages with the same group id will be processed fifo
        sqs.sendMessage(request);
    }

}
