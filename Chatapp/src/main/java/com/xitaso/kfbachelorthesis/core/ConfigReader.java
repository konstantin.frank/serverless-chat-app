package com.xitaso.kfbachelorthesis.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    private static ConfigReader configReader = null;

    private String cognitoUserPoolID;
    private String cognitoAppclientID;
    private String cognitoIdentityPoolID;
    private String region;
    private String apigatewayUrl;
    private String s3BucketName;

    public static ConfigReader getCongigReader() {
        if (configReader == null) {
            configReader = new ConfigReader();
        }
        return configReader;
    }

    private ConfigReader() {
        Properties properties = new Properties();
        InputStream input = null;

        try {
            System.out.println(System.getProperty("user.dir"));
            //input = new FileInputStream("src/main/resources/config.properties");
            input = getClass().getClassLoader().getResourceAsStream("config.properties");
            properties.load(input);

            cognitoUserPoolID = properties.getProperty("COGNITO_USER_POOL_ID");
            cognitoAppclientID = properties.getProperty("COGNITO_APPCLIENT_ID");
            cognitoIdentityPoolID = properties.getProperty("COGNITO_IDENTITY_POOL_ID");
            region = properties.getProperty("REGION");
            apigatewayUrl = properties.getProperty("APIGATEWAY_URL");
            s3BucketName = properties.getProperty("S3_BUCKET_NAME");

        } catch (IOException e) {
            System.out.println("Fatal Error: Could not read values from config.properies");
            //System.out.println("Required values: \nCOGNITO_USER_POOL_ID \nCOGNITO_APPCLIENT_ID\n COGNITO_IDENTITY_POOL_ID \nREGION");
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getCognitoUserPoolID() {
        return cognitoUserPoolID;
    }

    public String getCognitoAppclientID() {
        return cognitoAppclientID;
    }

    public String getCognitoIdentityPoolID() {
        return cognitoIdentityPoolID;
    }

    public String getRegion() {
        return region;
    }

    public String getApigatewayUrl() {
        return apigatewayUrl;
    }

    public String getS3BucketName() { return s3BucketName; }
}
