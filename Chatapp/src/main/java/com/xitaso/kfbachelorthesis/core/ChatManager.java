package com.xitaso.kfbachelorthesis.core;

import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.ChatMessage;
import com.xitaso.kfbachelorthesis.model.TextChatMessage;
import com.xitaso.kfbachelorthesis.model.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class ChatManager implements NewChatMessagesHandler {

    private static List<Chat> chats;
    private ReentrantLock chatListLock;

    public ChatManager(){
        chatListLock = new ReentrantLock();
    }

    public List<Chat> getChats() {
        return chats; // no immutable object or copy since this would result in too many objects
    }

    public void addChat(Chat chat) {
        chats.add(chat);
    }

    @Override
    public void handleNewChatMessage(ChatMessage message, String username) {
        lockChats();
        System.out.println("[CM] handling new chatmessage for user "+username+ " - the chatmessage was sent by current user: "+message.wasSentByCurrentUser());
        try {
            for (int i = 0; i < chats.size(); i++) {
                if (username.equals(chats.get(i).getUser().getUsername())) {
                    chats.get(i).addChatMessage(message);
                    System.out.println("[CM] Executing on chat message added");
                    Main.getActiveController().onChatMessageAdded(message);
                    return;
                }
            }
            // the chat with this user doesnt exist yet, so we have to create it
            User user = new User(username);
            user.setProfilePicture(Main.getS3FileEssentials().getProfilePicture(username));
            chats.add(new Chat(user));
            chats.get(chats.size() - 1).addChatMessage(message);
            Main.getActiveController().onChatMessageAdded(message);
        } finally {
           unlockChats();
        }
    }

    public void lockChats() {
        chatListLock.lock();
    }

    public void unlockChats() {
        chatListLock.unlock();

    }


    public void fetchChats() {

        System.out.println("JWT token: "+ Main.getAuthenticatedUser().getJWTToken());

        try {
            HttpsURLConnection connection = doHttpsCall();

            String jsonResponse = getJsonResult(connection);

            connection.disconnect();

            System.out.println(jsonResponse);

            this.chats = toChatList(jsonResponse);
            for (Chat chat: chats) {
                chat.eraseUnreadMessagesCount();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private HttpsURLConnection doHttpsCall() throws IOException {
        URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/chat/list?username=" + Main.getAuthenticatedUser().getUsername());
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
        //connection.setDoOutput(true);
        connection.getResponseCode();

        return connection;
    }

    private String getJsonResult(HttpsURLConnection connection) throws IOException{
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        } catch (IOException e) {
            throw e;
        } finally {
            if (in != null) {in.close();}
        }
    }

    private List<Chat> toChatList(String json) throws ParseException {
        HashMap<String, Chat> chatsHashMap = new HashMap<>(); // a hashmap for easier access to the different chats, see algorithm

        String authenticatedUsername = Main.getAuthenticatedUser().getUsername();

        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(json);
        JSONArray messagesJsonList = (JSONArray) jsonObject.get("messageList");
        int messageCount = Integer.parseInt(jsonObject.get("messageCount").toString());

        parseMessages(messageCount, messagesJsonList, authenticatedUsername, chatsHashMap);

        List<Chat> chatList = new ArrayList<>(chatsHashMap.values());
        Collections.sort(chatList);
        for (Chat chat: chatList) {
            chat.sortMessages();
        }
        return chatList;
    }

    private void parseMessages(int messageCount, JSONArray messagesJsonList, String authenticatedUsername, HashMap<String, Chat> chatsHashMap) throws ParseException{
        JSONParser parser = new JSONParser();
        for (int i = 0; i < messageCount; i++) {
            JSONObject jsonEncodedMessage = (JSONObject) parser.parse((String) messagesJsonList.get(i));

            String senderName = jsonEncodedMessage.get("senderName").toString();
            String receiverName = jsonEncodedMessage.get("receiverName").toString();
            Date dateSent = new Date(jsonEncodedMessage.get("dateSent").toString());
            //LocalDate dateSent = LocalDate.parse(jsonEncodedMessage.get("dateSent").toString());
            String text = jsonEncodedMessage.get("text").toString();

            ChatMessage message;

            if (senderName.equals(authenticatedUsername)) {
                // message was sent by oneself, so put it in the chat with its receiver
                message = new TextChatMessage(dateSent, true, text);
                addMessageToUserChat(receiverName, message, chatsHashMap);
            } else {
                // message was received by oneself, so put it in the chat with its sender
                message = new TextChatMessage(dateSent, false, text);
                addMessageToUserChat(senderName, message, chatsHashMap);
            }
        }
    }

    /**
     * @param username NOTE: this should NOT be the name of the current user
     */
    private void addMessageToUserChat(String username, ChatMessage message, HashMap<String, Chat> chatsHashMap) {
        if (!chatsHashMap.containsKey(username)) {
            // if the chat entry does not exist jet, we have to create it
            chatsHashMap.put(username, new Chat(new User(username)));
        }
        chatsHashMap.get(username).addChatMessage(message);
    }

}
