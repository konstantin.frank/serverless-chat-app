package com.xitaso.kfbachelorthesis.core;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.model.Credentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.ChatMessage;
import com.xitaso.kfbachelorthesis.model.TextChatMessage;
import javafx.application.Platform;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SQSMessageThread extends Thread {

    // poll intervals in ms - see run method for more details
    private final int POLL_INTERVAL_UPPER_BOUNDARY= 10000;
    private final int POLL_INTERVAL_NORMAL = 5000;
    private final int POLL_INTERVAL_LOWER_BOUNDARY = 1500;

    private AmazonSQS sqs;
    private String sqsQueueName;

    private NewChatMessagesHandler subscriber; // we will forward received messages to this subscriber

    public SQSMessageThread(NewChatMessagesHandler subscriber) {
        super();

        this.subscriber = subscriber;

        Credentials credentials = Main.getCognitoUserEssentials().getCredentials(
                Main.getAuthenticatedUser().getProvider(), Main.getAuthenticatedUser().getJWTToken());
        System.out.println(credentials.toString());

        sqs = AmazonSQSClientBuilder.standard()
                .withRegion("eu-west-1")
                .build();
        try {
            HttpsURLConnection connection = doHttpsCallToGetSQSQueryName();
            String jsonResult = getJsonResult(connection);
            connection.disconnect();

            sqsQueueName = getQueryName(jsonResult);
        } catch (IOException e){
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void run() {
        int pollInterval = POLL_INTERVAL_NORMAL;
        System.out.println("[SQSMT] Started message thread with queue "+sqsQueueName);
        while(true) {
            try {
                System.out.println("[SQSMT] Polling");
                sleep(pollInterval);
                ReceiveMessageRequest request = new ReceiveMessageRequest()
                        .withQueueUrl(sqsQueueName)
                        .withMaxNumberOfMessages(4)
                        .withAttributeNames("SentTimestamp")
                        .withMessageAttributeNames("senderName");
                ReceiveMessageResult result = sqs.receiveMessage(request);
                List<Message> sqsMessages = result.getMessages();
                for(Message msg: sqsMessages) {
                    System.out.println("[SQSMT] received " + msg.toString());
                    asyncInvokeSubscriber(msg);

                    // for performance improvements you could delete all the messages in a batch request
                    sqs.deleteMessage(sqsQueueName, msg.getReceiptHandle());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(TextChatMessage message, String receiverName) {
        try {
            int responsecode = doHttpsCallToSendMessage(message, receiverName);
            if (responsecode != 200) {
                System.out.println("Could not send message - error code " + responsecode);
            } else {
                subscriber.handleNewChatMessage(message, receiverName);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void asyncInvokeSubscriber(Message msg) {
        // we cannot update the UI from a different thread, so we invoke it this way
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // Update UI
                try {
                    invokeSubscriber(msg);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private HttpsURLConnection doHttpsCallToGetSQSQueryName() throws IOException {
        URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/chat/sqsqueue");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
        connection.getResponseCode();
        return connection;
    }

    private int doHttpsCallToSendMessage(ChatMessage message, String receiverName) throws IOException {
        System.out.println("[SQSMT] Sending message to "+receiverName);
        URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/chat/sendmessage");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
        connection.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        writer.write(encodeMessageToJson(message, receiverName));
        writer.flush();

        int responsecode = connection.getResponseCode();
        connection.disconnect();
        return responsecode;
    }

    private String encodeMessageToJson(ChatMessage message, String receiverName) {
        JSONObject json = new JSONObject();
        json.put("receiverName", receiverName);
        json.put("messageBody", ((TextChatMessage) message).getText());
        return json.toString();
    }

    private String getJsonResult(HttpsURLConnection connection) throws IOException{
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        } catch (IOException e) {
            throw e;
        } finally {
            if (in != null) {in.close();}
        }
    }

    private String getQueryName(String jsonString) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(jsonString);
        return (String) json.get("sqsQueueName");
    }


    private void invokeSubscriber(Message msg) throws ParseException {
        Date dateSent = new Date(Long.parseLong(msg.getAttributes().get("SentTimestamp")));
        String body = msg.getBody();
        String sendername = msg.getMessageAttributes().get("senderName").getStringValue();

        ChatMessage chatMessage = new TextChatMessage(dateSent, false, body);
        System.out.println("[SQSMT] invoking subscriber");
        subscriber.handleNewChatMessage(chatMessage, sendername);
    }
}
