package com.xitaso.kfbachelorthesis.core;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * reads the response of a HttpsUrlConnection
 */
public class HttpsURLConnectionStringParser {

    public static String getStringResult(HttpsURLConnection connection) throws IOException {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        } catch (IOException e) {
            throw e;
        } finally {
            if (in != null) {in.close();}
        }
    }
}
