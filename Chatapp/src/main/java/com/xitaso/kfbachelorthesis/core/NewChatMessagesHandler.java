package com.xitaso.kfbachelorthesis.core;

import com.xitaso.kfbachelorthesis.model.ChatMessage;
import com.xitaso.kfbachelorthesis.model.User;

public interface NewChatMessagesHandler {
    public void handleNewChatMessage(ChatMessage message, String username);
}
