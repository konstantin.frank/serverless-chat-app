package com.xitaso.kfbachelorthesis.core;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.model.Credentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.xitaso.kfbachelorthesis.main.Main;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

/**
 * Provides access to files stored on amazon s3 and contains methods to upload files (currently only the profile picture, but extension for other files is easy)
 *
 * Only invoke the constructor after the User is logged in!
 */
public class S3FileEssentials {

    private AmazonS3 s3Client;

    public S3FileEssentials() {
        Credentials credentials = Main.getCognitoUserEssentials().getCredentials(
                Main.getAuthenticatedUser().getProvider(), Main.getAuthenticatedUser().getJWTToken());
        s3Client = AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.fromName(ConfigReader.getCongigReader().getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicSessionCredentials(credentials.getAccessKeyId(), credentials.getSecretKey(), credentials.getSessionToken())))
                .build();
    }

    public String fetchProfilePicture() {
        return null;
    }

    public void uploadProfilePictureToS3(File profilePictureFile) {
        System.out.println("[S3E] Trying to POST image");
        HttpsURLConnection connection = null;

        try {
            URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/files/profilepicture");
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/pdf");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
            connection.setDoOutput(true);
            // copy the image to the connection output stream

            //BufferedImage image = ImageIO.read(profilePictureFile);
            //ImageIO.write(image, "png", connection.getOutputStream());
            connection.getOutputStream().write(encodeFileToBase64Binary(profilePictureFile).getBytes("UTF-8"));
            //Files.copy(profilePictureFile.toPath(), connection.getOutputStream());

            int responseCode = connection.getResponseCode();
            System.out.println("Received response code " + responseCode + " from connection");

        }  catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public Image getProfilePicture(String username) {
        System.out.println("[S3E] Trying to GET image for user "+username);
        String s3key = getProfilePictureS3Location(username);
        if (s3key == null) {
            // user has no profile picture yet
            return new Image(getClass().getResource("/images/male_icomoon.png").toExternalForm());
        }
        S3Object s3Object = s3Client.getObject(new GetObjectRequest(ConfigReader.getCongigReader().getS3BucketName(), s3key));
        return decodeBase64BinaryToImage(getStringFromS3Object(s3Object));
    }

    /**
     * Does an https call to get the s3bucket key of a users profile picture
     * @param username the user which profile picture location should be fetched
     * @return the s3bucket name if the user exists or null if no such user exists
     */
    private String getProfilePictureS3Location(String username) {
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/files/profilepicture?username=" + username);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
            int responseCode = connection.getResponseCode();

            String jsonString = HttpsURLConnectionStringParser.getStringResult(connection);
            JSONParser parser = new JSONParser();
            JSONObject jsonResponseBody = (JSONObject) parser.parse(jsonString);

            if (jsonResponseBody.get("userExisting").equals("false")) {
                return null;
            }

            return (String) jsonResponseBody.get("s3objectName");

        }  catch (IOException e) {
            e.printStackTrace();
        } catch(ParseException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    private static String encodeFileToBase64Binary(File file){
        String encodedfileString = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            fileInputStreamReader.read(bytes);
            encodedfileString = new String(Base64.encodeBase64(bytes), "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedfileString;
    }

    private static Image decodeBase64BinaryToImage(String base64encodedString){
        Image decodedImage = null;
        try {
            byte[] bytes = Base64.decodeBase64(base64encodedString);
            BufferedImage awtImage = ImageIO.read(new ByteArrayInputStream(bytes));
            Image image = SwingFXUtils.toFXImage(awtImage, null);
            return image;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getStringFromS3Object(S3Object object) {
        S3ObjectInputStream inputStream = object.getObjectContent();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String s3ObjectString = IOUtils.toString(bufferedReader);
            return s3ObjectString;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
