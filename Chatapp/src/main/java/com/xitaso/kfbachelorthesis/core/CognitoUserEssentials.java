package com.xitaso.kfbachelorthesis.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentity;
import com.amazonaws.services.cognitoidentity.AmazonCognitoIdentityClientBuilder;
import com.amazonaws.services.cognitoidentity.model.*;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.*;


public class CognitoUserEssentials {

    private String temporarySavedUsername = null;

    public CognitoUserEssentials() {

    }

    public Credentials getCredentials(String idprovider, String JWTtoken) {

        AnonymousAWSCredentials awsCreds = new AnonymousAWSCredentials();
        AmazonCognitoIdentity provider = AmazonCognitoIdentityClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.fromName(ConfigReader.getCongigReader().getRegion()))
                .build();
        GetIdRequest idrequest = new GetIdRequest();
        idrequest.setIdentityPoolId(ConfigReader.getCongigReader().getCognitoIdentityPoolID());
        idrequest.addLoginsEntry(idprovider, JWTtoken);
        GetIdResult idResult = provider.getId(idrequest);

        GetCredentialsForIdentityRequest request = new GetCredentialsForIdentityRequest();
        request.setIdentityId(idResult.getIdentityId());
        request.addLoginsEntry(idprovider, JWTtoken);

        GetCredentialsForIdentityResult result = provider.getCredentialsForIdentity(request);
        return result.getCredentials();
    }

    /**
     * Validates a cognito user by username and password
     * @return a JWT token if the validation was successfull, else null
     */
    public String validateUser(String username, String password) {
        AuthenticationHelper helper = new AuthenticationHelper(ConfigReader.getCongigReader().getCognitoUserPoolID(),
                ConfigReader.getCongigReader().getCognitoAppclientID(), "");
        return helper.PerformSRPAuthentication(username, password);
    }

    public void register(String username, String email, String password) throws Exception {
        AWSCognitoIdentityProvider cognitoIdentityProvider = buildAWSCognitoIdentityProvider();

        SignUpRequest signUpRequest = new SignUpRequest();

        signUpRequest.setClientId(ConfigReader.getCongigReader().getCognitoAppclientID());
        signUpRequest.setUsername(username);
        signUpRequest.setPassword(password);

        List<AttributeType> attributesList = new ArrayList<>();
        AttributeType emailAttribute = new AttributeType();
        emailAttribute.setName("email");
        emailAttribute.setValue(email);
        attributesList.add(emailAttribute);
        signUpRequest.setUserAttributes(attributesList);

        SignUpResult result = cognitoIdentityProvider.signUp(signUpRequest);
        System.out.println(result.toString());
    }

    public void confirmEmailAddress(String username, String confirmationCode) {
        AWSCognitoIdentityProvider cognitoIdentityProvider = buildAWSCognitoIdentityProvider();

        ConfirmSignUpRequest confirmSignUpRequest = new ConfirmSignUpRequest()
                .withClientId(ConfigReader.getCongigReader().getCognitoAppclientID())
                .withUsername(username)
                .withConfirmationCode(confirmationCode);

        ConfirmSignUpResult result = cognitoIdentityProvider.confirmSignUp(confirmSignUpRequest);
        System.out.println(result.toString());
    }

    public String ResetPassword(String username) {
        AWSCognitoIdentityProvider cognitoIdentityProvider = buildAWSCognitoIdentityProvider();

        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
        forgotPasswordRequest.setUsername(username);
        forgotPasswordRequest.setClientId(ConfigReader.getCongigReader().getCognitoAppclientID());
        ForgotPasswordResult forgotPasswordResult = new ForgotPasswordResult();

        forgotPasswordResult = cognitoIdentityProvider.forgotPassword(forgotPasswordRequest);
        temporarySavedUsername = username;

        return forgotPasswordResult.toString();
    }

    public String UpdatePassword(String newpassword, String code) {
        if (temporarySavedUsername == null) {
            return "error"; //TODO enhance this
        }

        AWSCognitoIdentityProvider cognitoIdentityProvider = buildAWSCognitoIdentityProvider();

        ConfirmForgotPasswordRequest confirmPasswordRequest = new ConfirmForgotPasswordRequest();
        confirmPasswordRequest.setUsername(temporarySavedUsername);
        confirmPasswordRequest.setPassword(newpassword);
        confirmPasswordRequest.setConfirmationCode(code);
        confirmPasswordRequest.setClientId(ConfigReader.getCongigReader().getCognitoAppclientID());
        ConfirmForgotPasswordResult confirmPasswordResult = new ConfirmForgotPasswordResult();

        confirmPasswordResult = cognitoIdentityProvider.confirmForgotPassword(confirmPasswordRequest);
        return confirmPasswordResult.toString();
    }

    private AWSCognitoIdentityProvider buildAWSCognitoIdentityProvider() {
        AnonymousAWSCredentials awsCredentials = new AnonymousAWSCredentials();
        AWSCognitoIdentityProvider cognitoIdentityProvider = AWSCognitoIdentityProviderClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withRegion(Regions.fromName(ConfigReader.getCongigReader().getRegion()))
                .build();
        return cognitoIdentityProvider;
    }
}
