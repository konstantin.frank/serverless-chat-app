package com.xitaso.kfbachelorthesis.core;

import com.xitaso.kfbachelorthesis.main.Main;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.URL;

public class ContactAdder {

    private ContactAdder() { }

    public static boolean checkIfUsernameExists(String username) {
        HttpsURLConnection connection = null;
        try {
            URL url = new URL(ConfigReader.getCongigReader().getApigatewayUrl() + "/user/exists?username=" + username);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", Main.getAuthenticatedUser().getJWTToken());
            int responseCode = connection.getResponseCode();

            String jsonString = HttpsURLConnectionStringParser.getStringResult(connection);
            JSONParser parser = new JSONParser();
            JSONObject jsonResponseBody = (JSONObject) parser.parse(jsonString);

            if (jsonResponseBody.get("userExisting").equals("false")) {
                return false;
            }
            return true;

        }  catch (IOException e) {
            e.printStackTrace();
        } catch(ParseException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return false;
    }
}
