package com.xitaso.kfbachelorthesis.model;

import javafx.scene.image.Image;

public class User {

    private Image profilePicture;
    private String username;

    public User(String username) {
        this.username = username;
    }

    public void setProfilePicture(Image profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Image getProfilePicture() {
        return profilePicture;
    }

    public String getUsername() {
        return username;
    }
}
