package com.xitaso.kfbachelorthesis.model;

public class AuthenticatedUser extends User{

    private String username;
    private String provider; // used for creating Credentials, see LoginController and CognitoUserEssentials
    private String JWTToken;

    public AuthenticatedUser(String username, String provider, String JWTToken) {
        super(username);
        this.username = username;
        this.provider = provider;
        this.JWTToken = JWTToken;
    }

    public String getUsername() {
        return username;
    }

    public String getProvider() {
        return provider;
    }

    public String getJWTToken() {
        return JWTToken;
    }

}
