package com.xitaso.kfbachelorthesis.model;

import java.time.LocalDate;
import java.util.Date;

public class TextChatMessage extends ChatMessage {

    private String text;

    public TextChatMessage(Date dateSent, boolean wasSentByCurrentUser, String text) {
        super(dateSent, wasSentByCurrentUser);
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
