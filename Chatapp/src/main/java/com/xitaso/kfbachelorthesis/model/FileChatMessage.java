package com.xitaso.kfbachelorthesis.model;

import java.time.LocalDate;
import java.util.Date;

public class FileChatMessage extends ChatMessage {
    public FileChatMessage(Date date) {
        super(date);
    }
}
