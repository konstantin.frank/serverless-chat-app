package com.xitaso.kfbachelorthesis.model;

import java.time.LocalDate;
import java.util.Date;

public abstract class ChatMessage implements Comparable<ChatMessage>{
    private Date dateSent;
    private boolean wasSentByCurrentUser; // true if message was sent by the authenticated user, false if he received the message

    public ChatMessage(Date dateSent) {
        this.dateSent = dateSent;
    }

    public ChatMessage(Date dateSent, boolean wasSentByCurrentUser) {
        this.dateSent = dateSent;
        this.wasSentByCurrentUser = wasSentByCurrentUser;
    }

    public int compareTo(ChatMessage chatMessage) {
        return (dateSent.compareTo(chatMessage.getDateSent()));
    }

    public Date getDateSent() {
        return dateSent;
    }

    public boolean wasSentByCurrentUser() {
        return wasSentByCurrentUser;
    }
}