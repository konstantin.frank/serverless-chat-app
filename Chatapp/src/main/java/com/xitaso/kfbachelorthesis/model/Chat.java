package com.xitaso.kfbachelorthesis.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Chat implements Comparable<Chat>{

    private User user;
    private List<ChatMessage> messages;
    private int unreadMessagesCount;

    public Chat(User user) {
        this.user = user;
        messages = new ArrayList<>();
        unreadMessagesCount = 0;
    }

    public void addChatMessage(ChatMessage message) {
        messages.add(message);
        unreadMessagesCount++;
    }

    public int compareTo(Chat chat) {
        return (user.getUsername().compareTo(chat.getUser().getUsername()));
    }

    public User getUser() {
        return user;
    }

    public List<ChatMessage> getMessages() {
        return messages; // since this can be a lot of data, we wont copy the list but give the original one
    }

    public int getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void eraseUnreadMessagesCount() {
        unreadMessagesCount = 0;
    }

    public void sortMessages() {
        Collections.sort(messages);
    }


}
