package com.xitaso.kfbachelorthesis.controller;

import com.amazonaws.services.cognitoidp.model.UsernameExistsException;
import com.xitaso.kfbachelorthesis.main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class RegisterController extends Controller {

    private final int MINIMUM_PASSWORD_LENGTH = 8;

    @FXML
    Text passWordMinimumLengthText;

    @FXML
    Text usernameAlreadyExistsText;

    @FXML
    Text passwordsDoNotMatchText;

    @FXML
    Text usernameMustNotContainHypensText;

    @FXML
    Label loginLink;

    @FXML
    TextField emailField;

    @FXML
    TextField usernameField;

    @FXML
    PasswordField passwordField;

    @FXML
    PasswordField confirmPasswordField;

    @FXML
    void onRegister(ActionEvent event) {
        passWordMinimumLengthText.setVisible(false);
        passwordsDoNotMatchText.setVisible(false);
        usernameMustNotContainHypensText.setVisible(false);
        usernameAlreadyExistsText.setVisible(false);

        String email = emailField.getText();
        String username = usernameField.getText();
        String password = passwordField.getText();

        // username must not contain hypens since we dont store the sqs queue names in a database, but search them with their prefix (they are of the form <username>-<some random string>)
        if(username.contains("-")) {
            usernameMustNotContainHypensText.setVisible(true);
            return;
        }

        if (password.length() < MINIMUM_PASSWORD_LENGTH) {
            passWordMinimumLengthText.setVisible(true);
            return;
        }

        if (!password.equals(confirmPasswordField.getText())){
            passwordsDoNotMatchText.setVisible(true);
            return;
        }

        try {
            Main.getCognitoUserEssentials().register(username, email, password); //TODO error-case
            VerifyController controller = (VerifyController) changeScene("verify.fxml");
            controller.setUsername(username);
        }
        catch (UsernameExistsException e) {
            usernameAlreadyExistsText.setVisible(true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void onMouseEnteredLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedLogin(MouseEvent event){
        changeScene("login.fxml");
    }
}
