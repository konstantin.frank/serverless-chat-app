package com.xitaso.kfbachelorthesis.controller;

import com.amazonaws.services.cognitoidp.model.CodeMismatchException;
import com.xitaso.kfbachelorthesis.main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class ResetPasswordController extends Controller {

    @FXML
    Text passwordsDoNotMatchText;

    @FXML
    Text codeWrongText;

    @FXML
    Label loginLink;

    @FXML
    TextField codeField;

    @FXML
    PasswordField passwordField;

    @FXML
    PasswordField passwordConfirmField;

    @FXML
    void onResetPassword(ActionEvent event) {
        codeWrongText.setVisible(false);
        passwordsDoNotMatchText.setVisible(false);

        String code = codeField.getText();
        String password = passwordField.getText();
        if (!password.equals(passwordConfirmField.getText())) {
            passwordsDoNotMatchText.setVisible(true);
            return;
        }
        try {
            Main.getCognitoUserEssentials().UpdatePassword(password, code); //TODO error-case
        } catch (CodeMismatchException e) {
            codeWrongText.setVisible(true);
            return;
        }
        changeScene("successfullResetPassword.fxml");
    }

    @FXML
    void onMouseEnteredLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedLogin(MouseEvent event){
        changeScene("login.fxml");
    }
}
