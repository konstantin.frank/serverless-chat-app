package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.ChatMessage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

import static com.xitaso.kfbachelorthesis.main.Main.primaryStage;

public abstract class Controller {

    public Controller changeScene(String fxml) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/" + fxml));
            Parent pane = loader.load();
            Controller controller = loader.getController();
            Main.setActiveController(controller);

            primaryStage.getScene().setRoot(pane);
            pane.requestFocus();
            return controller;

            //Parent pane = FXMLLoader.load(getClass().getResource("/" + fxml));
            //primaryStage.getScene().setRoot(pane);
            //pane.requestFocus();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void onChatMessageAdded(ChatMessage message) {
        // override this method if you need to react to the creation of chatmessages
    }
}
