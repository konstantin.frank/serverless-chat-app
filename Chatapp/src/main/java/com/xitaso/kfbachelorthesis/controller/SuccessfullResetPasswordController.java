package com.xitaso.kfbachelorthesis.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class SuccessfullResetPasswordController extends Controller {

    @FXML
    Label loginLink;

    @FXML
    void onMouseEnteredLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedLogin(MouseEvent event){
        changeScene("login.fxml");
    }
}
