package com.xitaso.kfbachelorthesis.controller;

import com.amazonaws.services.cognitoidp.model.CodeMismatchException;
import com.xitaso.kfbachelorthesis.main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class VerifyController extends Controller {

    private String username; // this private field is necessary to share information between the register and verify controller

    public void setUsername(String username) {
        this.username = username;
    }

    @FXML
    Label loginLink;

    @FXML
    TextField confirmationCodeTextField;

    @FXML
    Text wrongCodeText;

    @FXML
    void onMouseEnteredLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedLogin(MouseEvent event){
        changeScene("login.fxml");
    }

    @FXML
    void onMouseEnteredConfirmButton(MouseEvent event) { ((Button) event.getTarget()).setOpacity(0.7); }

    @FXML
    void onMouseExitedConfirmButton(MouseEvent event) { ((Button) event.getTarget()).setOpacity(1.0); }

    @FXML
    void onConfirmEmail(ActionEvent event) {
        wrongCodeText.setVisible(false);
        try {
            if (!confirmationCodeTextField.getText().isEmpty()) {
                Main.getCognitoUserEssentials().confirmEmailAddress(username, confirmationCodeTextField.getText());
                changeScene("login.fxml");
            }
        } catch (CodeMismatchException e) {
            wrongCodeText.setVisible(true);
        }
    }
}
