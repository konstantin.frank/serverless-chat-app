package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.ChatMessage;
import com.xitaso.kfbachelorthesis.model.TextChatMessage;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.Date;
import java.util.List;

public class ChatController extends Controller {

    private static final int CHAT_FORM_PADDING_VERTICAL = 15;
    private static final int CHAT_FORM_PADDING_HORIZONTAL = 15;
    private static final int CHAT_FORM_MAX_LENGTH_X = 480;
    private static final int CHAT_TEXT_PADDING = 15;
    private static final int CHAT_TEXT_FONT_SIZE = 14;

    // its not easy to retrieve the scrollbar width from a scroll pane, so I will use this "hack" to calculate the positions with this value
    private static  int CHAT_SCROLLBAR_WIDTH = 8; //18;

    private static final Color CHAT_TEXT_COLOR = Color.GRAY;
    private static final Color CHAT_COLOR_SELF = Color.web("#508448", 0.2);
    private static final Color CHAT_COLOR_BORDER_SELF = Color.web("#508448", 0.4);
    private static final Color CHAT_COLOR_OPPONENT = Color.web("#ffffff");
    private static final Color CHAT_COLOR_BORDER_OPPONENT = Color.web("#a1a1a1");

    private int currentChatPositionY; // the next y position of the chat bubbles
    private Chat chat;

    @FXML
    TextArea inputTextArea;

    @FXML
    Text usernameText;

    @FXML
    Text onlineText;

    @FXML
    AnchorPane anchorPane;

    @FXML
    ScrollPane scrollPane;

    @FXML
    Circle profilePictureContainer;

    @Override
    public void onChatMessageAdded(ChatMessage message) {
        drawChatMessage(message);
        chat.eraseUnreadMessagesCount();
        if (currentChatPositionY > anchorPane.getPrefHeight()) {
            anchorPane.setPrefHeight(currentChatPositionY);
        }

        // if we execute scrollPane.setVvalue immediately, were going to miss the update of the anchor pane
        // causing the last message not beeing scrolled down (however only when the message was received by us?)

        // Since this is a release-blocker it has to be fixed somehow.

        // Those nested Runnables are a wonderful example of how you should NOT fix bugs.
        // But honestly, those javaFX-problems are not important for this thesis, so I wont spend too much time on it.

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        // Update UI
                        scrollPane.setVvalue(1.0);
                    }
                });
            }
        };
        thread.start();
    }

    void initiateChat(Chat chat) {
        this.chat = chat;
        loadProfilePicture(chat);
        usernameText.setText(chat.getUser().getUsername());
        //onlineText.setText("Sat Jul 28 13:19:39 UTC 2018");
        Main.getChatManager().lockChats();
        try {
            drawChatMessages(chat.getMessages());
            scrollPane.setVvalue(1.0);
            chat.eraseUnreadMessagesCount();
        } finally {
            Main.getChatManager().unlockChats();
        }
    }

    private void loadProfilePicture(Chat chat) {
        profilePictureContainer.setFill(new ImagePattern(chat.getUser().getProfilePicture()));
    }

    private void drawChatMessages(List<ChatMessage> messages) {
        currentChatPositionY = CHAT_FORM_PADDING_VERTICAL;
        for (ChatMessage message: messages) {
            drawChatMessage(message);
        }
        if (currentChatPositionY > anchorPane.getPrefHeight()) {
            anchorPane.setPrefHeight(currentChatPositionY);
        }
    }

    private void drawChatMessage(ChatMessage message) {
        double messageTextWidth;
        double messageTextHeight;
        int chatRectangleX = CHAT_FORM_PADDING_HORIZONTAL;

        Text messageText = createMessageText(message, currentChatPositionY);
        messageTextHeight = wrapMessageIfNecessaryAndGetHeight(messageText);
        messageTextWidth = messageText.getBoundsInLocal().getWidth();

        chatRectangleX = mirrorTextIfNecessaryAndGetChatRectangleX(message, messageText, chatRectangleX);
        Rectangle chatRectangle = createChatRectangle(chatRectangleX, currentChatPositionY, messageTextWidth, messageTextHeight, message);

        anchorPane.getChildren().addAll(chatRectangle, messageText);

        currentChatPositionY += messageTextHeight + CHAT_TEXT_PADDING * 3;
    }

    private int mirrorTextIfNecessaryAndGetChatRectangleX(ChatMessage message, Text messageText, int oldChatRectangleX) {
        double chatRectangleX = oldChatRectangleX;
        if(message.wasSentByCurrentUser()) {
            //messageTextX = (int) Main.primaryStage.getWidth() - messageTextX - (int) messageTextWidth;
            double messageTextX = 595 - CHAT_FORM_PADDING_HORIZONTAL - messageText.getBoundsInLocal().getWidth() - CHAT_SCROLLBAR_WIDTH;
            messageText.setX(messageTextX);
            chatRectangleX = messageTextX - CHAT_TEXT_PADDING;
        }
        return (int) chatRectangleX;
    }

    private Rectangle createChatRectangle(int chatRectangleX, int chatRectangleY, double messageTextWidth, double messageTextHeight, ChatMessage message) {
        Rectangle chatRectangle = new Rectangle(chatRectangleX,
                chatRectangleY, messageTextWidth + CHAT_TEXT_PADDING * 2, messageTextHeight + CHAT_TEXT_PADDING * 2);
        chatRectangle.setStrokeWidth(0.5);
        chatRectangle.setArcHeight(20);
        chatRectangle.setArcWidth(20);
        if(message.wasSentByCurrentUser()) {
            chatRectangle.setFill(CHAT_COLOR_SELF);
            chatRectangle.setStroke(CHAT_COLOR_BORDER_SELF);
        } else {
            chatRectangle.setFill(CHAT_COLOR_OPPONENT);
            chatRectangle.setStroke(CHAT_COLOR_BORDER_OPPONENT);
        }
        return chatRectangle;
    }

    private double wrapMessageIfNecessaryAndGetHeight(Text messageText) {
        if (messageText.getBoundsInLocal().getWidth() >= CHAT_FORM_MAX_LENGTH_X - CHAT_TEXT_PADDING * 2 ) {
            messageText.setWrappingWidth(CHAT_FORM_MAX_LENGTH_X - CHAT_TEXT_PADDING * 2);
        }
        // the resizing abouve did eventually change the texts size!
        return messageText.getBoundsInLocal().getHeight();
    }

    private Text createMessageText(ChatMessage message, int currentY) {
        Text messageText = new Text(CHAT_FORM_PADDING_HORIZONTAL + CHAT_TEXT_PADDING,
                currentY + CHAT_TEXT_PADDING + CHAT_TEXT_FONT_SIZE, ((TextChatMessage) message).getText());
        //messageText.maxWidth(CHAT_FORM_MAX_LENGTH_X - CHAT_TEXT_PADDING * 2);
        messageText.setFont(new Font(Font.getDefault().getName(),CHAT_TEXT_FONT_SIZE));
        messageText.setFill(CHAT_TEXT_COLOR);
        return messageText;
    }


    @FXML
    void onMouseClickedBackImage(MouseEvent event) {
        ChatListOverviewController controller = (ChatListOverviewController) changeScene("chatListOverview.fxml");
        controller.drawChats();
    }

    @FXML
    void onMouseClickedOpenSettings(MouseEvent event) {
        SettingsController controller = (SettingsController) changeScene("settings.fxml");
        controller.initSettings();
    }

    @FXML
    void onMouseClickedSend(MouseEvent event) {
        String textToSend = inputTextArea.getText();
        System.out.println("[CC] Sending message to "+chat.getUser().getUsername()+" "+textToSend);
        TextChatMessage message = new TextChatMessage(new Date(),true,textToSend);
        Main.getSqsMessageThread().sendMessage(message, chat.getUser().getUsername());
        inputTextArea.clear();
    }

    @FXML
    void onMouseEnteredSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }

    @FXML
    void onMouseEnteredBackImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedBackImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }

    @FXML
    void onMouseEnteredSend(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedSend(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }
}
