package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.core.CognitoJWTParser;
import com.xitaso.kfbachelorthesis.core.S3FileEssentials;
import com.xitaso.kfbachelorthesis.core.SQSMessageThread;
import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.AuthenticatedUser;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.User;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import org.json.JSONObject;

/**
 * ATTENTION: much of the application logic happens on onLogin()!
 */
public class LoginController extends Controller {

    @FXML
    Text usernameOrPasswordWrongText;

    @FXML
    Label registerLink;

    @FXML
    Label forgotPasswordLink;

    @FXML
    TextField usernameField;

    @FXML
    PasswordField passwordField;

    @FXML
    void onLogin(ActionEvent event) {
        usernameOrPasswordWrongText.setVisible(false);
        String username = usernameField.getText();
        String password = passwordField.getText();
        String JWTToken = Main.getCognitoUserEssentials().validateUser(username, password);
        if (JWTToken == null) {
            System.out.println("Password or Username wrong");
            usernameOrPasswordWrongText.setVisible(true);
            return;
        }

        // we'll use runLater to let the "username or password wrong"-text vanish
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                JSONObject payload = CognitoJWTParser.getPayload(JWTToken); // this is the parser from org.json, not from org.json.simple (like everywhere else)!
                String provider = payload.get("iss").toString().replace("https://", "");

                // The following order is important: the authenticated user must be set first and the s3 file essentials must be initiated before the chats are drawn.
                // The message thread should be started last.

                Main.setAuthenticatedUser(new AuthenticatedUser(username, provider, JWTToken));
                System.out.println("[LC] SUCCESSFULL LOGIN OF USER "+Main.getAuthenticatedUser().getUsername());
                Main.getChatManager().fetchChats();
                Main.primaryStage.setHeight(900);
                Main.initS3FileEssentials();

                for (Chat chat: Main.getChatManager().getChats()) {
                    Image profilePicture = Main.getS3FileEssentials().getProfilePicture(chat.getUser().getUsername());
                    chat.getUser().setProfilePicture(profilePicture);
                }
                Image ownProfilePicture = Main.getS3FileEssentials().getProfilePicture(Main.getAuthenticatedUser().getUsername());
                Main.getAuthenticatedUser().setProfilePicture(ownProfilePicture);

                ChatListOverviewController controller = (ChatListOverviewController) changeScene("chatListOverview.fxml");
                controller.drawChats();
                SQSMessageThread sqsMessageThread = new SQSMessageThread(Main.getChatManager());
                // todo if some kind of IO is implemented, like caching, you should use a different way to stop threads when the program is closed!
                sqsMessageThread.setDaemon(true);
                Main.setSqsMessageThread(sqsMessageThread);
                sqsMessageThread.start();
            }
        });
    }

    @FXML
    void onMouseEnteredRegister(MouseEvent event){
        registerLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedRegister(MouseEvent event){
        registerLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedRegister(MouseEvent event){
        changeScene("register.fxml");
    }

    @FXML
    void onMouseEnteredForgotPassword(MouseEvent event){
        forgotPasswordLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedForgotPassword(MouseEvent event){
        forgotPasswordLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedForgotPassword(MouseEvent event){
        changeScene("forgotPassword.fxml");
    }

    @FXML
    void onMouseEnteredLoginButton(MouseEvent event) {
        ((Button) event.getSource()).setOpacity(0.85);
    }

    @FXML
    void onMouseExitedLoginButton(MouseEvent event) {
        ((Button) event.getSource()).setOpacity(1.0);
    }
}
