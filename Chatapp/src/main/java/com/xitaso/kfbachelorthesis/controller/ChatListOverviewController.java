package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.core.ChatManager;
import com.xitaso.kfbachelorthesis.core.NewChatMessagesHandler;
import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.ChatMessage;
import com.xitaso.kfbachelorthesis.model.TextChatMessage;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChatListOverviewController extends Controller {

    private static final int CHAT_TEASER_START_X = 0;
    private static final int CHAT_TEASER_WIDTH = 610;
    private static final int CHAT_TEASER_HEIGHT = 80;
    private static final Color CHAT_TEASER_COLOR_DEFAULT = Color.web("#508448", 0);
    private static final Color CHAT_TEASER_COLOR_SELECTED = Color.web("#508448", 0.2);

    private static final int PROFILE_PICTURE_PADDING_X = 25;
    private static final int PROFILE_PICTURE_SIZE = 65;

    private static final int USERNAME_PADDING_X_RELATIVE = 25;
    private static final int USERNAME_PADDING_Y_RELATIVE = 30;
    private static final int USERNAME_FONT_SIZE = 20;
    private static final Color USERNAME_FONT_COLOR = Color.web("#545454");

    private static final int LAST_MESSAGE_FONT_SIZE = 14;
    private static final int LAST_MESSAGE_PADDING_Y_RELATIVE = 25;
    private static final Color LAST_MESSAGE_FONT_COLOR = Color.web("#a1a1a1");

    private static final int UNREAD_MESSAGES_OFFSET_X = 548;
    private static final int UNREAD_MESSAGES_OFFSET_Y = 25;
    private static final int UNREAD_MESSAGES_CIRCLE_SIZE = 28;
    private static final Color UNREAD_MESSAGES_CIRCLE_COLOR = Color.web("#fa8072");
    private static final Font UNREAD_MESSAGES_FONT = new Font("Courier New", 14);

    private static final double LINE_SEPARATOR_STENGTH = 0.5;
    private static final Color LINE_SEPARATOR_COLOR = Color.web("#cdcdcd");

    private HashMap<Rectangle, Integer> rectangleChatMapper = new HashMap<>();

    @FXML
    ImageView settingsImage;

    @FXML
    ImageView newChatImage;

    @FXML
    ScrollPane scrollPane;

    @FXML
    AnchorPane anchorPane;

    void drawChats() {
        List<Chat> chats = Main.getChatManager().getChats();
        System.out.println("[CLOVC] Trying to lock chats");
        Main.getChatManager().lockChats();
        System.out.println("[CLOVC] locked chats");

        try {
            for (int i = 0; i < chats.size(); i++) {
                drawChatTeaser(chats.get(i), i);
            }
        } finally {
            System.out.println("[CLOVC] unlocking chats");
            Main.getChatManager().unlockChats();
        }
    }

    private void drawChatTeaser(Chat chat, int number) {
        Rectangle clickParent = createClickParentRectangle(number);
        Circle profilePicture = createProfilePictureCircle(chat, number);
        Text username = createUsernameText(chat, number);
        Text lastMessage = createLastMessageTeaserText(chat, number);
        Line separator = createSeparatorLine(number);

        anchorPane.getChildren().addAll(clickParent, profilePicture, username, lastMessage, separator);

        if (chat.getUnreadMessagesCount() != 0) {
            anchorPane.getChildren().add(createUnreadMessagesStackpane(chat, number));
        }

    }

    private Rectangle createClickParentRectangle(int number) {
        Rectangle clickParent = new Rectangle(-4, calcRectangleY(number), CHAT_TEASER_WIDTH, CHAT_TEASER_HEIGHT);
        clickParent.setFill(CHAT_TEASER_COLOR_DEFAULT);
        clickParent.setStrokeWidth(0);

        rectangleChatMapper.put(clickParent, number);

        clickParent.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((Rectangle) event.getSource()).setFill(CHAT_TEASER_COLOR_SELECTED);
            }
        });
        clickParent.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ((Rectangle) event.getSource()).setFill(CHAT_TEASER_COLOR_DEFAULT);
            }
        });
        clickParent.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ChatController controller = (ChatController) changeScene("chat.fxml");
                //find the chat for the rectangle that was clicked
                int index = rectangleChatMapper.get((Rectangle) event.getSource());
                controller.initiateChat(Main.getChatManager().getChats().get(index));
            }
        });
        return clickParent;
    }

    private Circle createProfilePictureCircle(Chat chat, int number) {
        Circle profilePicture = new Circle(calcProfilePictureCenterX(), calcProfilePictureCenterY(number), PROFILE_PICTURE_SIZE / 2);
        profilePicture.setSmooth(true); // use better scaling algorithm
        //profilePicture.setFitHeight(PROFILE_PICTURE_SIZE);
        //profilePicture.setFitWidth(PROFILE_PICTURE_SIZE);
        //profilePicture.setFill(new ImagePattern(new Image(getClass().getResource("/images/ferdo.jpg").toExternalForm())));
        profilePicture.setFill(new ImagePattern(chat.getUser().getProfilePicture()));
        profilePicture.setStrokeWidth(0);
        profilePicture.setMouseTransparent(true);
        return profilePicture;
    }

    private Text createUsernameText(Chat chat, int number) {
        Text username = new Text(calcUsernameX(), calcUsernameY(number), chat.getUser().getUsername());
        username.setFont(new Font(Font.getDefault().getName(), USERNAME_FONT_SIZE));
        username.setFill(USERNAME_FONT_COLOR);
        username.setMouseTransparent(true);
        return  username;
    }

    private Text createLastMessageTeaserText(Chat chat, int number) {
        String lastMessageTeaser = "";
        if (!chat.getMessages().isEmpty()) {
            lastMessageTeaser = ((TextChatMessage) chat.getMessages().get(chat.getMessages().size() - 1)).getText();
            // cut the string to avoid that it reaches the edge
            if (lastMessageTeaser.length() > 60) {
                // better: use the javafx Text object lastMessage to display the right size
                lastMessageTeaser = lastMessageTeaser.substring(0, 60) + "...";
            }
        }
        Text lastMessage = new Text(calcUsernameX(), calcLastMessageY(number), lastMessageTeaser);
        lastMessage.setFont(new Font(Font.getDefault().getName(),LAST_MESSAGE_FONT_SIZE));
        lastMessage.setFill(LAST_MESSAGE_FONT_COLOR);
        lastMessage.setMouseTransparent(true);
        return lastMessage;
    }

    private StackPane createUnreadMessagesStackpane(Chat chat, int number) {
        Circle unreadMessagesCircle = new Circle();
        unreadMessagesCircle.setRadius(UNREAD_MESSAGES_CIRCLE_SIZE / 2);
        unreadMessagesCircle.setFill(UNREAD_MESSAGES_CIRCLE_COLOR);
        unreadMessagesCircle.setStrokeWidth(0);
        unreadMessagesCircle.setMouseTransparent(true);

        Text unreadMessagesText = new Text(Integer.toString(chat.getUnreadMessagesCount()));
        unreadMessagesText.setFont(UNREAD_MESSAGES_FONT);
        unreadMessagesText.setFill(Color.WHITE);
        unreadMessagesText.setBoundsType(TextBoundsType.VISUAL);
        unreadMessagesText.setStyle("-fx-font-weight: bold");
        unreadMessagesText.setMouseTransparent(true);

        StackPane unreadMessagesStackPane = new StackPane(unreadMessagesCircle, unreadMessagesText);
        unreadMessagesStackPane.setLayoutX(UNREAD_MESSAGES_OFFSET_X);
        unreadMessagesStackPane.setLayoutY(calcUnreadMessagesStackPaneY(number));
        unreadMessagesStackPane.setMouseTransparent(true);
        return  unreadMessagesStackPane;
    }

    private Line createSeparatorLine(int number) {
        Line separator = new Line(0, calcSeparatorY(number), CHAT_TEASER_WIDTH, calcSeparatorY(number));
        separator.setStrokeWidth(LINE_SEPARATOR_STENGTH);
        separator.setStroke(LINE_SEPARATOR_COLOR);
        return separator;
    }
    /**
     * @param number starts with 0!
     */
    private int calcRectangleY(int number) {
        return CHAT_TEASER_START_X + number * CHAT_TEASER_HEIGHT;
    }

    private int calcProfilePictureCenterX() {
        return PROFILE_PICTURE_PADDING_X + PROFILE_PICTURE_SIZE / 2;
    }

    private int calcProfilePictureCenterY(int number) {
        return calcRectangleY(number) + (CHAT_TEASER_HEIGHT - PROFILE_PICTURE_SIZE) /2 + PROFILE_PICTURE_SIZE / 2;
    }

    private int calcUsernameY(int number) {
        return calcRectangleY(number) + USERNAME_PADDING_Y_RELATIVE;
    }

    private int calcUsernameX() {
        return PROFILE_PICTURE_PADDING_X + PROFILE_PICTURE_SIZE + USERNAME_PADDING_X_RELATIVE;
    }

    private int calcLastMessageY(int number) {
        return calcUsernameY(number) + LAST_MESSAGE_PADDING_Y_RELATIVE;
    }

    private int calcUnreadMessagesStackPaneY(int number) {
        return calcRectangleY(number) + UNREAD_MESSAGES_OFFSET_Y;
    }

    private int calcSeparatorY(int number) {
        return calcRectangleY(number) +  CHAT_TEASER_HEIGHT;
    }

    @FXML
    void onMouseClickedAddChat(MouseEvent event) {
        changeScene("addContact.fxml");
    }

    @FXML
    void onMouseClickedOpenSettings(MouseEvent event) {
        SettingsController controller = (SettingsController) changeScene("settings.fxml");
        controller.initSettings();
    }

    @FXML
    void onMouseEnteredSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }

    @FXML
    void onMouseEnteredNewChatImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedNewChatImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }

    @Override
    public void onChatMessageAdded(ChatMessage message) {
        // of course it would make far more sense just to UPDATE them.
        ChatListOverviewController controller = (ChatListOverviewController) changeScene("chatListOverview.fxml");
        controller.drawChats();
    }
}
