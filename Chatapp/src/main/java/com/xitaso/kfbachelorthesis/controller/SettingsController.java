package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.main.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;

import java.io.*;


public class SettingsController extends Controller {

    @FXML
    Label usernameLabel;

    @FXML
    Circle profileCircle;


    public void initSettings() {
        usernameLabel.setText(Main.getAuthenticatedUser().getUsername());
        profileCircle.setFill(new ImagePattern(Main.getAuthenticatedUser().getProfilePicture()));
    }

    @FXML
    void onMouseClickedBackImage(MouseEvent event) {
        ChatListOverviewController controller = (ChatListOverviewController) changeScene("chatListOverview.fxml");
        controller.drawChats();
    }

    @FXML
    void onMouseEnteredBackImage(MouseEvent event) { ((ImageView) event.getTarget()).setOpacity(0.7); }

    @FXML
    void onMouseExitedBackImage(MouseEvent event) { ((ImageView) event.getTarget()).setOpacity(1.0); }

    @FXML
    void onMouseClickedDeleteAccount(MouseEvent event) {
        // todo
    }

    @FXML
    void onMouseEnteredDeleteAccount(MouseEvent event) { ((Button) event.getTarget()).setOpacity(0.7); }

    @FXML
    void onMouseExitedDeleteAccount(MouseEvent event) { ((Button) event.getTarget()).setOpacity(1.0); }

    @FXML
    void onMouseClickedEditProfilePicture(MouseEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG Images (*.png)", "*.png"));
        fileChooser.setTitle("Select profile picture");
        File profilePictureFile = fileChooser.showOpenDialog(Main.getPrimaryStage());
        try {
            if (profilePictureFile != null) {
                InputStream fileInputStream = new FileInputStream(profilePictureFile);
                Main.getS3FileEssentials().uploadProfilePictureToS3(profilePictureFile);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Image newProfilePicture = Main.getS3FileEssentials().getProfilePicture(Main.getAuthenticatedUser().getUsername());
                Main.getAuthenticatedUser().setProfilePicture(newProfilePicture);
                profileCircle.setFill(new ImagePattern(newProfilePicture));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void onMouseEnteredEditProfilePicture(MouseEvent event) { ((ImageView) event.getTarget()).setOpacity(0.4); }

    @FXML
    void onMouseExitedEditProfilePicture(MouseEvent event) { ((ImageView) event.getTarget()).setOpacity(0.7); }
}
