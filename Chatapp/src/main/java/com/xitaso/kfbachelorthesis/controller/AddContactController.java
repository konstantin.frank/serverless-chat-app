package com.xitaso.kfbachelorthesis.controller;

import com.xitaso.kfbachelorthesis.core.ChatManager;
import com.xitaso.kfbachelorthesis.core.ContactAdder;
import com.xitaso.kfbachelorthesis.main.Main;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;


public class AddContactController extends Controller {

    @FXML
    TextField usernameTextField;

    @FXML
    Text cannotAddYourselfText;

    @FXML
    public void onAddContact(ActionEvent event) {
        cannotAddYourselfText.setVisible(false);
        String username = usernameTextField.getText();
        if (Main.getAuthenticatedUser().getUsername().equals(username)) {
            cannotAddYourselfText.setVisible(true);
            return;
        }
        if (ContactAdder.checkIfUsernameExists(username)) {
            ChatController controller = (ChatController) changeScene("chat.fxml");
            User user = new User(username);
            user.setProfilePicture(Main.getS3FileEssentials().getProfilePicture(username));
            Chat chat = new Chat(user);
            Main.getChatManager().addChat(chat);
            controller.initiateChat(chat);
        }
    }

    @FXML
    public void onMouseEnteredAddContact(MouseEvent event) {
        ((Button) event.getSource()).setOpacity(0.85);
    }

    @FXML
    public void onMouseExitedAddContact(MouseEvent event) {
        ((Button) event.getSource()).setOpacity(1.0);
    }

    @FXML
    void onMouseClickedOpenSettings(MouseEvent event) {
        SettingsController controller = (SettingsController) changeScene("settings.fxml");
        controller.initSettings();
    }

    @FXML
    void onMouseClickedBackImage(MouseEvent event) {
        ChatListOverviewController controller = (ChatListOverviewController) changeScene("chatListOverview.fxml");
        controller.drawChats();
    }

    @FXML
    void onMouseEnteredSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedSettingsImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }

    @FXML
    void onMouseEnteredBackImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(0.7);
    }

    @FXML
    void onMouseExitedBackImage(MouseEvent event) {
        ((ImageView) event.getTarget()).setOpacity(1.0);
    }
}
