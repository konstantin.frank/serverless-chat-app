package com.xitaso.kfbachelorthesis.controller;

import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.xitaso.kfbachelorthesis.main.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

public class ForgotPasswordController extends Controller {

    @FXML
    Text usernameDoesNotExistText;

    @FXML
    Label loginLink;

    @FXML
    TextField usernameField;

    @FXML
    void onForgotPassword(ActionEvent event) {
        usernameDoesNotExistText.setVisible(false);
        try {
            Main.getCognitoUserEssentials().ResetPassword(usernameField.getText()); //TODO error-case
        } catch (UserNotFoundException e) {
            usernameDoesNotExistText.setVisible(true);
            return;
        }

        // we use run later in order to let the error message vanish
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                changeScene("resetPassword.fxml");
            }
        });
    }

    @FXML
    void onMouseEnteredLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: true");
    }

    @FXML
    void onMouseExitedLogin(MouseEvent event){
        loginLink.setStyle("-fx-underline: false");
    }

    @FXML
    void onMouseClickedLogin(MouseEvent event){
        changeScene("login.fxml");
    }
}
