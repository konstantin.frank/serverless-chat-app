package com.xitaso.kfbachelorthesis.main;

import com.xitaso.kfbachelorthesis.controller.Controller;
import com.xitaso.kfbachelorthesis.core.ChatManager;
import com.xitaso.kfbachelorthesis.core.S3FileEssentials;
import com.xitaso.kfbachelorthesis.core.SQSMessageThread;
import com.xitaso.kfbachelorthesis.model.AuthenticatedUser;
import com.xitaso.kfbachelorthesis.model.Chat;
import com.xitaso.kfbachelorthesis.model.User;
import com.xitaso.kfbachelorthesis.core.CognitoUserEssentials;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.List;

/**
 * ATTENTION: much of the application logic happens AFTER a successful login, see LoginController.onLogin()
 */

public class Main extends Application {

    public static Stage primaryStage;
    private static Controller activeController;
    private static CognitoUserEssentials cognitoUserEssentials; // helper class that abstracts from Amazon Cognito SDK
    private static AuthenticatedUser validatedUser = null; // the current validated user (in oder to share him between stages)
    private static ChatManager chatManager;
    private static SQSMessageThread sqsMessageThread;
    private static S3FileEssentials s3FileEssentials;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/login.fxml"));
        primaryStage.setTitle("Serverless chat app");
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/images/chat-yannick.png")));
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 595, 500));
        root.requestFocus();
        primaryStage.show();
        cognitoUserEssentials = new CognitoUserEssentials();
        chatManager = new ChatManager();
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static S3FileEssentials getS3FileEssentials() {
        return s3FileEssentials;
    }

    public static void initS3FileEssentials() {
        s3FileEssentials = new S3FileEssentials();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static CognitoUserEssentials getCognitoUserEssentials() {
        return cognitoUserEssentials;
    }

    public static AuthenticatedUser getAuthenticatedUser() {
        return validatedUser;
    }

    public static void setAuthenticatedUser(AuthenticatedUser newValidatedUser) {
        validatedUser = newValidatedUser;
    }

    public static ChatManager getChatManager() {
        return chatManager;
    }

    public static SQSMessageThread getSqsMessageThread() {
        return sqsMessageThread;
    }

    public static void setSqsMessageThread(SQSMessageThread sqsMessageThread) {
        Main.sqsMessageThread = sqsMessageThread;
    }

    public static Controller getActiveController() {
        return activeController;
    }

    public static void setActiveController(Controller activeController) {
        Main.activeController = activeController;
    }
}
