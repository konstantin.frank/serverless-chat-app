# Serverless Architecture with AWS

Table of contents:
1. General information
2. Deployment and build instructions (backend)
3. Build instructions (client)
4. Deleting the stack
5. Troubleshooting
6. Manual setup of the backend


## GENERAL INFORMATION

This project was part of my Bachelor thesis.

All used services are eglegible for Amazons Free Tier if you have set up your account recently or are going to do it for this repository.
(However, costs of this backend should gernerally be very low)

## DEPLOYMENT AND BUILD INSTRUCTIONS (BACKEND)

1. Prequesites
	- I suggest using Intellij (I used community version 18.1.5).
	- You will need the following intellij plugins: Gradle, AWS Cloudformation
	- Download the repository to your local disk.
	- Configure your IDE to use the Oracle Java 8 SDK (different Java versions will NOT work!).

2. Configuration of the AWS account AWS CLI
	- Create an AWS account: https://aws.amazon.com/de/resources/create-account/?nc1=h_ls (Note that there is a button with "create free account" on the bottom
	- Never publish your AWS access keys, especially not to plattforms like github.
	- AWS will probably give you some security instructions, e.g. dont use your root account but create some IAM roles. Its up to you to follow them, but it would probably make sense.
	- Note: The following steps assume that you have administrator access.
	
You can achieve this by adding the following policy to your iam role:

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}


3. Install the AWS CLI on your computer: https://aws.amazon.com/de/cli/
	- Windows users will need to execute a msi installer file, linux users might use their package manager
	- Test the installation: Open a terminal and type "aws --version" (without the quotes)
	- Configure AWS: type "aws configure" in the terminal. You will have to provide your IAM-login data. For default region name, type eu-central-1 (this is Frankfurt). Default output format is not important.

4. Deploying the serverless backend
	- Open the ServerlessBackend project with InteliJ
	- Open the build.gradle file in the ServerlessBackend folder. You NEEED to change the following definitions or the deployment process will fail (see file for more explanation):
		- in line "def deploymentBucketName = ..." change the deployment bucket name
		- in line "def projectName" change the project name
		- This is due to the fact that s3-bucket-names have to be globally unique across all accounts and such things. (Off course we could just insert some random sting in the name or something like that, but its defenitly important to keep this fact in mind!) You will also have to change them if you want to deploy this template with a different account.
	- Open a terminal (you can use the one from intelij) and change your working directory to the ServerlessBackend folder
	- Run "gradlew" (without the quotes, off course)
	- Run "gradlew deploy"
	- The build should succeed now.
	- If you log in to the aws management console https://aws.amazon.com/de/console/ select Cloudformation in the services list. Now you should see your newly created stack (if you dont, change your region (on the top left) to Frankfurt (eu-central-1). Any occured errors will be visible if you click on the stack.
	
Your backend is now ready!

Note:

* A Cloudformation stack can only be updated after a failed deployment that results in ROLLBACK_COMPLETE if there was a successfull deployed state before the rollback. If you create a new stack and the setup process fails, you have to **delete the stack** before you can upload it again.
* If you cant find your AWS resources on the AWS management console (their online portal), you might have the wrong region setting - switch regions on the top right. Many resource instances are region-specific.

* You can check the structure of your template using the AWS CLI command cloudformation validate-template (make sure you work in the Backend directory)
```
aws cloudformation validate-template --template-body file://./cloudformation.yaml
```
* You dont need to upload all lambda artifacts every time you want to update the template. Rewrite the build.gradle file to use a specific value for date or look it up in the s3 bucket and then use the 'aws cloudformation deploy' command like this: (make sure you work in the serverless backend directory and note that this time the file is accessed differently than in the previous command)
```
aws cloudformation deploy --template-file ./cloudformation.yaml --stack-name serverless-chat-1 --capabilities CAPABILITY_NAMED_IAM --parameter-overrides DeploymentBucketName="serverless-chat-deployment-1" DeploymentTime=20180825231406 ProjectVersion="1.0" ProjectName="serverless-chat-1"
```
If you dont want to store old artifacts, you can edit the timestamp in the gradle script to be always the same one.
Then the files should be overwritten. 
If you dont, maybe clean the bucket sometimes up - every deployment stores about 70MB on the bucket.

## BUILDING INSTRUCTIONS (CLIENT)

Prequesites:
* Best use IntelliJ (the community edition is sufficient) with the Marven plugin.
* Download and install the Java 8 SDK and use it in your IDE (different Java versions will NOT work! Make sure that the client is executed with java 8).
* The client was only tested on Windows 10. It should run with Mac OS, but Linux users might get problems due to Java FX.
* Go to the AWS Management Console and select CloudFormation in the service list. Select the stack that you just created when its ready.
* Select 'Outputs' (one of the first listed options even above 'Events').
* Copy those values to the right keys in the config.property file in the client directory Chatapp/src/main/resources. There must be no white spaces after these values!

* Open a terminal or use Intellijs one, change your location to the Chatapp directory and run 'mvn package'. Your client should now be ready!

**NOTE**
* Any interactions with the app will be quite slow at the start. This is due to the lambda functions performing a cold start.
* You can only send messages to users who logged in at least once.
* After you register and log in for the first time, creating your sqs queue will take some seconds - messages sent in this timeframe will be lost.
* There is no real error handling implemented in the client.

## DELETING THE STACK

It might probably be a good idea to delete the stack if you're done, because dynamodb tables e.g. do have a price for upkeeping capacity.

CloudFormation does not support deleting s3 buckets that are not empty (to avoid acidentally deleting those files, I guess)
Therefore, you have to select the user file bucket (its named like <project-name>-usefiles) in the management console and delete all files there.
(Of course this step could also be automated by a custom AWS resouce that invokes a lambda that does the cleanup for you)

In order to delete the stack, simply go to AWS management console and choose Cloudformation in the service list. Then click on your stack and choose 'delete stack'.

The deployment bucket (ehere we stored our lambda jars) was not created as part of the stack, but by the gradle deploy script.
You have to delete it manually by using the management console or the AWS CLI:
```
aws s3api delete-bucket --bucket your-bucket-name --region eu-central-1 --force
```
The sqs queues (every user is related to such a queue) were also not created by the template.
Since the chat currently does not support deleting of a registered account, no automated delete functionality is included for them.
However, you can easily delete them in the Management Console on the page of the sqs-service.
You will have to change your region on the upper right to Ireland, since FIFO-queues were not supported in Frankfurt when this thesis was written.
Then all the queues will be listed. (A Custom CloudFormation resource for cloudformation could also do the job)

## TROUBESHOOTING

Common errors:
* If there is a java exception that JAXB is not available, check if you are actually using Java 8.
* If the Gradle script for deploying the backend exits with a BucketAlreadyExists-exception, you have to choose a different unique project name (just setting the number at the end to 2 will probably not work!)
* If your cliet doesn't seem to work properly, check if you pasted the parameters in the config.properties file with a space or a tab charakter at the end of the lines and remove them.

## MANUAL SETUP OF THE BACKEND

If you wish to try a manual setup without the template (to see how time consuming this is or to get a better idea of what the template does) here are some instructions:
Make sure that you add everything in the region Frankfurt!

1. You will need an AWS account - see Deployment and Build instructions. Open the AWS management console and make sure you are using the Frankfurt region.
2. First create the dynamoDB-tables:
	- Select DynamoDB in the service list on the top left
	- Create a table for the messages - the table name is up to you, but the primary key has to be SenderName (case sensitive). DO NOT CLICK CREATE TABLE YET.
	- Click on "add sort key" and add the sort key DateSent.
	- Deactivate "Use default settings"
	- Deactivate auto-sacling for read and write capacity and select 4 for read and 2 for write if you just want to test it
	- Click on Create
	- Click on your created table and select Indices
	- Create a new index, name it "ReceiverNameIndex" and type "ReceiverName" as the primary key (case-sensitive). Leave all other settings at default and create it.	
	- Create a table for the users - the table name is up to you, but the primary key has to be Username (as always, case-sensitive)
	- Deactivate "use default settings" 
	- Deactivate auto-sacling for read and write capacity and select 2 for read and 1 for write if you just want to test it
	- Click on Create
	
2. Create the s3-bucket: (since we wont upload jar files to s3 to upload them to lambda from there, we only need a file bucket for our users)	
	- Select S3 in the service list on the top left
	- Click on create bucket and specify a name for your bucket. It must be globally unique!
	- Set Frankfurt (eu-central-1) as region and click create. No additional settings required.
	
3. Next create the IAM roles:
	- Click on the 'services' tab on the top left
	- Select IAM
	- I wont list all role names that you have to create, instead have a look at the template. You have to create every role that is listed there, use the name that was used in the template. Detailed instructions for creating a role:
	- Click create role, select AWS service and then Lambda (not EC2). Click on "next: permissions" (probably on the right bottom)
	- Add the AWSLambdaBasicExecutionPolicy from the dropdown for every role except the one for cognito users.
	- Inline policies will be added later - click now on "next: review" and enter the name as it is stated in the template. Create the role.
	- Now go to the summary screen of the role and click on "add inline policy" (mid-top right side at the table)
	- see what permissions are listed in the template and transfer them to the role you are creating. If you select a service in the drop down menu, you can search for permissions.
	- For the resources that should be granted access to see the template. You might need to enter an arn of bucket or table:
		- The table arn can be found on the dynamodb page on the overview screen of the table on the bottom.
		- The s3-bucket arn can be found if you click on the s3 bucket (not the name, just the line) and click on copy arn on the overview that opens on the right side. You have to add "/*" at the end of the arn.
	- repeat this process for all roles. Have fun!
	
4. Create the lambda functions:
  	- Click on the 'services' tab on the top left
	- Select Lambda
	- Build the serverlessBackend jars with the cmd command 'gradlew build' in the Serverlessbackend directory
	- Create all lambda functions (except lambda-cloudformation-custom-logic) like they are described in the template (Timeout, RAM size, java8, role, handler class).
	- For each function upload the jar that is named like lambda-name-1.0-all.jar from the lambda/build/lib directorys on the management console
	- There might be a notification that the handler class cannot be found - ignore it, this is simply wrong.
	- For the environment variables:
	    - the PROJECT_NAME variables should be a lowercase word (and must be the same in both functions!)
		- REGION is "eu-central-1" (case sensitive!)
		- USER_TABLE_NAME is the name of your users dynamodb table (not the arn!). You find the name in the dynamodb overview -> Tables.
		- MESSAGE_TABLE_NAME: as above, but of the messages table
		- SECONDARY_INDEX_NAME is "ReceiverNameIndex" (case sensitive!)
		- FILE_BUCKET_NAME is the name of the user file bucket you created in step 2
	- dont forget to click on sav (top right) before you exit the function overview	
	
5. Create the Cognito User Pool:
	- Click on the 'services' tab on the top left
	- Select Cognito, then select 'manage user pools'
	- Create a new one. Choose a name that you like and 'step through settings'
	- Leave the first page as-is (Users sign in with username, email is the only standard attribute to be required, no custom attributes)
	- On the next page, leave the password lenght at 8 but deselect all options there (you can of course leave then enabled, but then you should put some verification logic in the client)
	- Leave 'Allow users to sign themself up' as it is (selected) and continue to next page
	- Leave MfA disabled and only email for autoverification, go to next page
	- Leave all next pages like they are and go to the overview after you created the pool
	- Now create an appclient (select some name and leave everything at default)
	- Go to the triggers section on the overview list, click on edit and add The 'AddConfirmedUserToDatabase'-Lambda or whatever you named it at Post confirmation
	- On the left side, select 'Domain Name' and choose a amazon Cognito domain (select any mane that you like and that is accepted by cognito) and save
	
6. Create the identity pool:
	- Click on the 'services' tab on the top left
	- Select Cognito, then select 'manage identity pools'
	- Create a new pool and name it how you like it.
	- Click on Authentication providers and add the user pool id and the app client id (you find them at the overview of the cognito user pool and the app client overview that can be reached from the user pool overview)	
	- Click create pool and add the AuthenticatedCognitoUser role at the 'authenticated role' field
	
7. Create the APIGateway:
    - Click on the 'services' tab on the top left
	- Select APIGateway
	- Create a new API (from scratch, dont select a template) with a name you like
	- Now create an authorizer (see left panel)
	- Choose a name that you like, select cognito type, choose the user pool you created in step 5 and type "Authorization" in the Toke Source field (leave the Token Validation field free), click create
	- Now create the resources and methods (click on the actions-button and then on create method / resource) like this (the resource and method names must be the same!). 
	    - For each Method: (1) click on the 'Method request' property and choose your Authorizer that you have just created (type is cognito authorizers) (dont forget to click on the small save button next to it). 
		- (2) click on the integration request, select Lambda, select Use Lambda Proxy Integration (important!), select eu-central-1 region and choose the right lambda (leave everything else at default): 
```
		   /
		     /chat
			        /list
				       GET  (ListChatsLambda)
				/sendmessage
					   POST (SendMessageLambda)
				/sqsqueue
					   GET  (CreateSQSQueueLambda)
			 /files
			        /profilepicture
					   GET  (GetProfilePictureLambda)
					   POST (UploadProfilePictureLambda)
			 /user  
			        /exists
					   GET  (GetContactLambda)
```		   
					   
Now deploy the apy (Actions button -> deploy Api) and choose prod as stage name.
	
8. Paste the config properties to the config.properties file like described in "Building instructions (client)" (however, you will have to search for the values manually:)
    - Cognito User pool id: The same id as used in step 6
	- Cognito Appclient id: The same id as used in step 6
	- Cognito Identitiy pool id: The id from the identity pool created in step 6 (click on 'edit identity pool to be able to see it!)
	- Region: eu-central-1
	- ApiGatewayUrl: can be found on top of your newly deployed stage overview from step 7("Invoke Url")
	- S3BucketName: the name of youruser files bucket from step 2
	
If you wish to delete the resources you just created (and you should do so, because DynamoDB tables have an upkeep cost):
Well, you have to do that manually for each ressource you just created. Have fun! (And dont forget the sqs queues that are in the **Ireland** region, not in Frankfurt!
